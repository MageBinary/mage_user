<?php
/**
 * Create password form.
 */
function mage_user_create_password_form($form, &$form_state, $account) {
  global $user;
  $register = ($user->uid > 0 ? FALSE : TRUE);

  // Get the currently logged in user object.
  $form['#account'] = $account;

  if (!isset($form_state['user'])) {
    $form_state['user'] = $account;
  }
  else {
    $account = $form_state['user'];
  }

  $form['#user'] = $account;

  // Display password field only for existing users or when user is allowed to
  // assign a password during registration.

  // Password confirm field.
  $form['account']['pass'] = array(
    '#type' => 'password_confirm',
    '#size' => 25,
    // '#title' => t('New Password'),
    '#required' => TRUE,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Continue'),
  );

  return $form;
}

/**
 * Submit handler for mage_user_create_password_form().
 */
function mage_user_create_password_form_submit(&$form, &$form_state) {
  // Set up the edit array to pass to user_save().
  $edit = array('pass' => $form_state['values']['pass']);

  // Save the account with the new password.
  user_save($form['#account'], $edit);

  // Inform the user.
  drupal_set_message(t('Your password has been changed.'));
  //redirect
  drupal_goto('user/me/create-profile');
}

/**
 * Create Fernmark Profile Form.
 */
function mage_user_create_profile_form($form, &$form_state, $account, $category = 'account') {
  global $user;

  // During initial form build, add the entity to the form state for use during
  // form building and processing. During a rebuild, use what is in the form
  // state.
  if (!isset($form_state['user'])) {
    $form_state['user'] = $account;
  }
  else {
    $account = $form_state['user'];
  }

  // @todo Legacy support. Modules are encouraged to access the entity using
  //   $form_state. Remove in Drupal 8.
  $form['#user'] = $account;
  $form['#user_category'] = $category;

  if ($category == 'account') {
    user_account_form($form, $form_state);
    // Attach field widgets.
    $langcode = entity_language('user', $account);
    field_attach_form('user', $account, $form, $form_state, $langcode);
  }

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  $form['#validate'][] = 'mage_user_create_profile_form_validate';
  // Add the final user profile form submit handler.
  $form['#submit'][] = 'mage_user_create_profile_form_submit';

  return $form;
}

/**
 * Validation function for the user create profile form.
 */
function mage_user_create_profile_form_validate($form, &$form_state) {
  entity_form_field_validate('user', $form, $form_state);
}

/**
 * Submit function for the user create profile form.
 */
function mage_user_create_profile_form_submit($form, &$form_state) {
  $account = $form_state['user'];
  $category = $form['#user_category'];
  // Remove unneeded values.
  form_state_values_clean($form_state);

  // Before updating the account entity, keep an unchanged copy for use with
  // user_save() later. This is necessary for modules implementing the user
  // hooks to be able to react on changes by comparing the values of $account
  // and $edit.
  $account_unchanged = clone $account;

  entity_form_submit_build_entity('user', $account, $form, $form_state);

  // Populate $edit with the properties of $account, which have been edited on
  // this form by taking over all values, which appear in the form values too.
  $edit = array_intersect_key((array) $account, $form_state['values']);

  user_save($account_unchanged, $edit, $category);
  $form_state['values']['uid'] = $account->uid;

  if ($category == 'account' && !empty($edit['pass'])) {
    // Remove the password reset tag since a new password was saved.
    unset($_SESSION['pass_reset_'. $account->uid]);
  }
  // Clear the page cache because pages can contain usernames and/or profile information:
  cache_clear_all();

  drupal_set_message(t('Your changes have been saved.'));

  //redirect
  drupal_goto('user/me/done-profile');
}

/**
 * account setting form.
 */
function mage_user_account_form($form, &$form_state, $account) {
  global $user;
  $register = ($user->uid > 0 ? FALSE : TRUE);

  // Get the currently logged in user object.
  $form['#account'] = $account;

  if (!isset($form_state['user'])) {
    $form_state['user'] = $account;
  }
  else {
    $account = $form_state['user'];
  }

  $form['#validate'][] = 'mage_user_account_form_validate';
  $form['#user'] = $account;

  // Display password field only for existing users or when user is allowed to
  // assign a password during registration.
  if (!$register) {
    // To skip the current password field, the user must have logged in via a
    // one-time link and have the token in the URL.
    $pass_reset = isset($_SESSION['pass_reset_' . $account->uid]) && isset($_GET['pass-reset-token']) && ($_GET['pass-reset-token'] == $_SESSION['pass_reset_' . $account->uid]);
    $protected_values = array();
    $current_pass_description = '';
    // The user may only change their own password without their current
    // password if they logged in via a one-time login link.
    if (!$pass_reset) {
      $protected_values['mail'] = isset($form['account']['mail']['#title']) ? $form['account']['mail']['#title'] : '';
      $protected_values['pass'] = t('Password');
      $request_new = l(t('Request new password'), 'user/password', array('attributes' => array('title' => t('Request new password via email.'))));
      $current_pass_description = t('!request_new.', array('!request_new' => $request_new));
    }

    // The user must enter their current password to change to a new one.
    if ($user->uid == $account->uid) {
      // Textfield for current password confirmation.
      $form['current_pass'] = array(
        '#type' => 'password',
        '#title' => t('Current password'),
        '#size' => 25,
        '#required' => TRUE,
        '#attributes' => array('autocomplete' => 'off'),
        '#access' => !empty($protected_values),
        '#description' => $current_pass_description,
        '#weight' => -10,
      );
      if(!empty($protected_values)) {
        $form['#validate'][] = 'mage_user_validate_current_pass';
      }
    }

    //comapny email fielf
    $contact_us = l(t('Contact us'), 'contact-us', array('attributes' => array('title' => t('Change email via contact us page.'))));
    $form['account']['mail'] = array(
      '#type' => 'textfield',
      '#title' => t('Company Email'),
      '#maxlength' => EMAIL_MAX_LENGTH,
      '#description' => t('All emails from the system will be sent to this address. The email address is not made public and will only be used if you wish to receive a new password or wish to receive certain news or notifications by email. Need to change this? !contact_us', array('!contact_us' => $contact_us)),
      '#required' => TRUE,
      '#default_value' => (!$register ? $account->mail : ''),
      '#weight' => -5,
    );

    // Password confirm field.
    $form['account']['pass'] = array(
      '#type' => 'password_confirm',
      '#size' => 25,
      '#required' => TRUE,
    );
  }
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Validate handler for change_pwd_page_form().
 */
function mage_user_validate_current_pass(&$form, &$form_state) {
  // Make sure the password functions are present.
  require_once DRUPAL_ROOT . '/' . variable_get('password_inc', 'includes/password.inc');

  // Make sure the provided current password is valid for this account.
  if (!user_check_password($form_state['values']['current_pass'], $form['#account'])) {
    form_set_error('current_pass', t('The current password you provided is incorrect.'));
  }
}

/**
 * Form validation handler for mage_user_account_form().
 *
 * @see mage_user_account_form()
 */
function mage_user_account_form_validate($form, &$form_state) {
  $account = $form['#user'];

  // Trim whitespace from mail, to prevent confusing 'email not valid'
  // warnings often caused by cutting and pasting.
  $mail = trim($form_state['values']['mail']);
  form_set_value($form['account']['mail'], $mail, $form_state);

  // Validate the email address, and check if it is taken by an existing user.
  if ($error = user_validate_mail($form_state['values']['mail'])) {
    form_set_error('mail', $error);
  }
  elseif ((bool) db_select('users')->fields('users', array('uid'))->condition('uid', $account->uid, '<>')->condition('mail', db_like($form_state['values']['mail']), 'LIKE')->range(0, 1)->execute()->fetchField()) {
    // Format error message dependent on whether the user is logged in or not.
    if ($GLOBALS['user']->uid) {
      form_set_error('mail', t('The email address %email is already taken.', array('%email' => $form_state['values']['mail'])));
    }
    else {
      form_set_error('mail', t('The email address %email is already registered. <a href="@password">Have you forgotten your password?</a>', array('%email' => $form_state['values']['mail'], '@password' => url('user/password'))));
    }
  }
}

/**
 * Submit handler for mage_user_create_password_form().
 */
function mage_user_account_form_submit(&$form, &$form_state) {
  // Set up the edit array to pass to user_save().
  $edit = array('pass' => $form_state['values']['pass'],
    'mail' => $form_state['values']['mail']);

  // Save the account with the new password.
  user_save($form['#account'], $edit);

  // Inform the user.
  drupal_set_message(t('Your changes have been saved.'));
  //redirect
  drupal_goto('user/me');
}

/**
 * Reset password form.
 */
function mage_user_reset_password_form($form, &$form_state, $account) {
  global $user;
  $register = ($user->uid > 0 ? FALSE : TRUE);

  // Get the currently logged in user object.
  $form['#account'] = $account;

  if (!isset($form_state['user'])) {
    $form_state['user'] = $account;
  }
  else {
    $account = $form_state['user'];
  }

  $form['#user'] = $account;

  // Display password field only for existing users or when user is allowed to
  // assign a password during registration.

  // Password confirm field.
  $form['account']['pass'] = array(
    '#type' => 'password_confirm',
    '#size' => 25,
    // '#title' => t('New Password'),
    '#required' => TRUE,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Submit handler for mage_user_reset_password_form().
 */
function mage_user_reset_password_form_submit(&$form, &$form_state) {
  // Set up the edit array to pass to user_save().
  $edit = array('pass' => $form_state['values']['pass']);

  // Save the account with the new password.
  user_save($form['#account'], $edit);

  // Inform the user.
  drupal_set_message(t('Your password has been changed.'));
  //redirect
  drupal_goto('user/me');
}