<div class="grey-bg">
    <div class="container" id="user-register">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 text-center header-content">
                    <h1 class="uppercase">Create Your FernMark Licence Profile</h1>
                    <h5>To promote and validate your FernMark Licence Accreditation on the <a href="/licensees" target="_blank" class="link-light">FernMark Licensees page</a>, please provide the information outlined below. You will then be taken to the Marketing Toolkit page where you can access the FernMark Licence logo artwork files. This includes both English and Mandarin versions.</h5>
                    <h5><a href="" class="link-light" data-toggle="modal" data-target="#company-example">Here's an example</a> of a finished profile on the Licensees Page.</h5>
                    <h5><a href="/contact-us" target="_blank" class="link-light">Need help?</a></h5>
                </div>
            </div>
        <!-- Modal -->
        <div class="modal fade" id="company-example" tabindex="-1" role="dialog" aria-labelledby="companyExampleLabel">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <a data-toggle="modal" href="#" class="close" data-dismiss="modal"><span>&times;</span></a>
                        <h4 class="modal-title visibility-hidden">company title</h4>
                    </div>
                    <div class="modal-body row">
                        <div class="col-xs-12 col-sm-12 col-md-10 col-md-offset-1 company-content">
                            <div class="row">
                                <div class="col-sm-12 col-md-7 col-lg-7">
                                    <h1 class="uppercase">company name</h1>
                                    <h5><b><a href="">www.companywebsite.co.nz</a></b></h5>
                                    <h5>Company overview - your 50 word blurb describing your business will go here.</h5>
                                </div>
                                <div class="col-sm-12 col-md-5 col-lg-5">
                                    <a href="#" target="_blank">
                                    <img src="/sites/all/themes/FernMark/assets/images/feature-image.png" alt="content-image" class="content-img img-responsive"></a>
                                </div>
                            </div>
                            <div class="product-block">
                                <h3>PRODUCTS</h3>
                                <ul>
                                    <li class="title">Example Category 1</li>
                                    <li>Example product 1</li>
                                    <li>Example product 2</li>
                                    <li>Example product 3</li>
                                    <li>Example product 4</li>
                                    <li>Example product 5</li>
                                </ul>
                                <ul>
                                    <li class="title">Example Category 2</li>
                                    <li>Example product 1</li>
                                    <li>Example product 2</li>
                                    <li>Example product 3</li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container application-form register-form user-profile-edit">
    <div class="row">
        <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <?php print drupal_render_children($form); ?>
                </div>
            </div>
        </div>
    </div>
</div>