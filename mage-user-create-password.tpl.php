<div class="grey-bg">
    <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 text-center login-header-content">
                    <h1 class="uppercase">Create your password</h1>
                    <h5>Once you've created your password. you can then set up your company profile which we will publish on the FernMark websites.</h5>
                </div>
            </div>
    </div>
</div>
<div class="container application-form register-form user-create-password">
    <div class="row">
        <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <?php print drupal_render_children($form) ?>
                </div>
            </div>
        </div>
    </div>
</div>