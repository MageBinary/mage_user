<?php

/**
 * Implements hook_menu().
 */
function mage_user_menu() {
  //create password page
  $items['user/%user/create-password'] = array(
    'title' => 'Create Your Password',
    'description' => 'Create Password Form',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('mage_user_create_password_form', 1),
    'access callback' => 'mage_user_edit_access',
    'access arguments' => array(1),
    'type' => MENU_LOCAL_TASK,
    'file' => 'mage-user.pages.inc',
  );

  //create your fernmark profile page
  $items['user/%user/create-profile'] = array(
    'title' => 'Create Your Fernmark Profile',
    'description' => 'Create Fernmark Profile Form',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('mage_user_create_profile_form', 1),
    'access callback' => 'user_edit_access',
    'access arguments' => array(1),
    'type' => MENU_LOCAL_TASK,
    'file' => 'mage-user.pages.inc',
  );

  //after submit create profile form
  $items['user/%user/done-profile'] = array(
    'title' => 'Thanks For Your Details',
    'description' => 'Profile Done',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('mage_user_done_profile_form', 1),
    'access callback' => 'user_view_access',
    'access arguments' => array(1),
    'type' => MENU_LOCAL_TASK,
    'file' => 'mage-user.pages.inc',
  );

  //Login detail page
  $items['user/%user/edit-account'] = array(
    'title' => 'Account Settings',
    'description' => 'Change User Account',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('mage_user_account_form', 1),
    'access callback' => 'user_edit_access',
    'access arguments' => array(1),
    'type' => MENU_LOCAL_TASK,
    'file' => 'mage-user.pages.inc',
  );

  //reset password page
  $items['user/%user/reset-password'] = array(
    'title' => 'Reset Your Password',
    'description' => 'Reset Password Form',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('mage_user_reset_password_form', 1),
    'access callback' => 'mage_user_edit_access',
    'access arguments' => array(1),
    'type' => MENU_LOCAL_TASK,
    'file' => 'mage-user.pages.inc',
  );
  return $items;
}

/**
 * Access callback for user create or reset password.
 */
function mage_user_edit_access($account) {
  return ((($GLOBALS['user']->uid == $account->uid) && isset($_SESSION['pass_reset_' . $account->uid])) || user_access('administer users')) && $account->uid > 0;
}

/**
 * Implements hook_form_FORM_ID_alter().
 * Remove the password and email field from the user_profile_form form
 * (user/%/edit).
 */
function mage_user_form_user_profile_form_alter(&$form, &$form_state) {
  // Hide the current password fields.
  $form['account']['pass_value']['#access'] = FALSE;
  $form['account']['pass']['#access'] = FALSE;
  $form['account']['current_pass']['#access'] = FALSE;
  $form['account']['mail']['#type'] = 'hidden';
}

/**
 * Implements hook_form_FORM_ID_alter().
 * Remove the password and email field from the mage_user_create_profile_form form
 * (user/%/create-profile).
 */
function mage_user_form_mage_user_create_profile_form_alter(&$form, &$form_state) {
  // Hide the current password fields.
  $form['account']['pass_value']['#access'] = FALSE;
  $form['account']['pass']['#access'] = FALSE;
  $form['account']['current_pass']['#access'] = FALSE;
  $form['account']['mail']['#type'] = 'hidden';
}

/**
 * Implements hook_form_FORM_ID_alter().
 * change password to new password for mage_user_account_form form
 * (user/%/edit-account).
 */
function mage_user_form_mage_user_account_form_alter(&$form, &$form_state, $form_id){

  $form['account']['pass']['#process'] = array('form_process_password_confirm', 'mage_user_password_confirm_process', 'user_form_process_password_confirm');
}

/**
 * Implementation of mage_user_password_confirm_process.
 */
function mage_user_password_confirm_process($element) {
  $element['pass1']['#title'] = t("New Password");
  $element['pass1']['#description'] = t("Create a new password for your account");
  return $element;
}

/**
 * Implements hook_form_FORM_ID_alter().
 * change password to new password for mage_user_create_password_form form
 * (user/%/create-password).
 */
function mage_user_form_mage_user_create_password_form_alter(&$form, &$form_state, $form_id){

  $form['account']['pass']['#process'] = array('form_process_password_confirm', 'mage_user_create_password_confirm_process', 'user_form_process_password_confirm');
}

/**
 * Implementation of mage_user_create_password_confirm_process.
 */
function mage_user_create_password_confirm_process($element) {
  $element['pass1']['#title'] = t("New Password");
  return $element;
}

/**
 * Implements hook_menu_alter.
 * Change page arguments to call custom function for user reset password menu item.
 */
function mage_user_menu_alter(&$items) {
  $items['user/reset/%/%/%']['page arguments'] = array('mage_user_user_pass_reset', 2, 3, 4);
}

/**
 * Menu callback; process one time login link and redirects to the password page on
 * success.
 */
function mage_user_user_pass_reset($form, &$form_state, $uid, $timestamp, $hashed_pass, $action = NULL) {
  global $user;

  // When processing the one-time login link, we have to make sure that a user
  // isn't already logged in.
  if ($user->uid) {
    // The existing user is already logged in.
    if ($user->uid == $uid) {
      drupal_set_message(t('You are logged in as %user. <a href="!user_edit">Change your password.</a>', array('%user' => $user->name, '!user_edit' => url("user/$user->uid/change-password"))));
    }
    // A different user is already logged in on the computer.
    else {
      $reset_link_account = user_load($uid);
      if (!empty($reset_link_account)) {
        drupal_set_message(t('Another user (%other_user) is already logged into the site on this computer, but you tried to use a one-time link for user %resetting_user. Please <a href="!logout">logout</a> and try using the link again.',
          array('%other_user' => $user->name, '%resetting_user' => $reset_link_account->name, '!logout' => url('user/logout'))));
      }
      else {
        // Invalid one-time link specifies an unknown user.
        drupal_set_message(t('The one-time login link you clicked is invalid.'));
      }
    }
    drupal_goto();
  }
  else {
    // Time out, in seconds, until login URL expires. Defaults to 24 hours =
    // 86400 seconds.
    $timeout = variable_get('user_password_reset_timeout', 86400);
    $current = REQUEST_TIME;
    // Some redundant checks for extra security ?
    $users = user_load_multiple(array($uid), array('status' => '1'));
    if ($timestamp <= $current && $account = reset($users)) {
      // No time out for first time login.
      if ($account->login && $current - $timestamp > $timeout) {
        drupal_set_message(t('You have tried to use a one-time login link that has expired. Please request a new one using the form below.'));
        drupal_goto('user/password');
      }
      elseif ($account->uid && $timestamp >= $account->login && $timestamp <= $current && $hashed_pass == user_pass_rehash($account->pass, $timestamp, $account->login)) {
        // Set the new user.
        $user = $account;
        // user_login_finalize() also updates the login timestamp of the
        // user, which invalidates further use of the one-time login link.
        user_login_finalize();
        watchdog('user', 'User %name used one-time login link at time %timestamp.', array('%name' => $account->name, '%timestamp' => $timestamp));
        drupal_set_message(t('You have just used your one-time login link. It is no longer necessary to use this link to log in. Please change your password.'));
        // Let the user's password be changed without the current password
        // check.
        $token = drupal_random_key();
        $_SESSION['pass_reset_' . $user->uid] = $token;

        if ($account->access == 0){
          // go directly to create password page
          drupal_goto('user/' . $user->uid . '/create-password', array('query' => array('pass-reset-token' => $token)));
        } else {
          // go directly to reset password page
          drupal_goto('user/' . $user->uid . '/reset-password', array('query' => array('pass-reset-token' => $token)));
        }
      }
      else {
        drupal_set_message(t('You have tried to use a one-time login link that has either been used or is no longer valid. Please request a new one using the form below.'));
        drupal_goto('user/password');
      }
    }
    else {
      // Deny access, no more clues.
      // Everything will be in the watchdog's URL for the administrator to
      // check.
      drupal_access_denied();
      drupal_exit();
    }
  }
}

/**
 * Implements hook_theme.
 * Change page template
 */
function mage_user_theme() {
    $items = array();

    $items['user_login'] = array(
        'render element' => 'form',
        'template' => 'mage-user-login',
    );
    $items['mage_user_account_form'] = array(
        'render element' => 'form',
        'template' => 'mage-user-edit-account',
    );
    $items['mage_user_create_password_form'] = array(
        'render element' => 'form',
        'template' => 'mage-user-create-password',
    );
    $items['mage_user_create_profile_form'] = array(
        'render element' => 'form',
        'template' => 'mage-user-create-profile',
    );
    $items['mage_user_done_profile_form'] = array(
        'render element' => 'form',
        'template' => 'mage-user-done-profile',
    );
    $items['mage_user_reset_password_form'] = array(
        'render element' => 'form',
        'template' => 'mage-user-reset-password',
    );
    return $items;
}

/**
 * Implements hook_admin_paths_alter.
 * custom page show tab in admin
 */
function mage_user_admin_paths_alter(&$paths) {
  $paths['user/*/create-password'] = 1;
  $paths['user/*/create-profile'] = 1;
  $paths['user/*/done-profile'] = 1;
  $paths['user/*/edit-account'] = 1;
}

/**
 * Implements hook_menu_local_tasks_alter.
 * hide create password/create profile tabs
 */
function mage_user_menu_local_tasks_alter( & $data) {
    foreach($data['tabs'][0]['output'] as $key => $value) {
        if ($value['#link']['path'] == "user/%/create-profile" || $value['#link']['path'] == "user/%/create-password") {
            unset($data['tabs'][0]['output'][$key]);
        }
    }
}

/**
 * Implements hook_form_FORM_ID_alter().
 * change e-mail to email
 * (user/password).
 */
function mage_user_form_user_pass_alter(&$form, &$form_state) {
  $form['name']['#title'] = 'Username or email address';
  $form['actions']['submit']['#value'] = 'email new password';
}