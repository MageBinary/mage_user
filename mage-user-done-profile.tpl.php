<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 text-center login-header-content">
            <h1 class="uppercase">Thanks for your details</h1>
            <h5>If you have completed your company profile in full, it will take 48 hours to be moderated before going live on the website. You can now access the Marketing Toolkit.</h5>
            <div class="links">
                <a class="btn btn-default btn-go-back" href="/toolkit">Marketing Toolkit</a>
            </div>
        </div>
    </div>
</div>