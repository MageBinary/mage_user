<?php if ($base_url == NULL) {
    global $base_url;
} ?>
<div class="login-title noselect">LOGIN</div>
<hr class="login-hr">
<?php print drupal_render_children($form) ?>
<div class="forgot-password">
    <a href="<?php print $base_url . "/user/password";?>">FORGOT PASSWORD?</a>
</div>