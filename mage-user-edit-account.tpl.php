<div class="grey-bg">
    <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 text-center login-header-content">
                    <h1 class="uppercase">Account Settings</h1>
                </div>
            </div>
    </div>
</div>
<div class="container application-form register-form user-edit-account">
    <div class="row">
        <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <?php print drupal_render_children($form) ?>
                </div>
            </div>
        </div>
    </div>
</div>